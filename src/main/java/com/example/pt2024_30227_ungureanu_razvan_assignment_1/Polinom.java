package com.example.pt2024_30227_ungureanu_razvan_assignment_1;


import java.util.*;
import java.util.stream.Collectors;

public class Polinom {


    private LinkedHashMap<Integer,Double> pol=new LinkedHashMap<>();



    public LinkedHashMap<Integer, Double> getPol() {
        return pol;
    }

    public void addMonom(Monom monom){
        int power= monom.getPutere();
        double coef= monom.getCoef();
        if(pol.containsKey(power)){
            coef=coef+pol.get(power);
        }

        pol.put(power,coef);
    }

    public void setPol(LinkedHashMap<Integer, Double> pol) {
        this.pol = pol;
    }

    @Override
    public String toString() {
        TreeMap<Integer,Double>polSortat=new TreeMap<>(Comparator.reverseOrder());
        Monom monomS=new Monom();
        for (Map.Entry<Integer, Double> entry : pol.entrySet()){
            int power = entry.getKey();
            double coef = entry.getValue();
            polSortat.put(power,coef);

        }
        pol.clear();
        for (Map.Entry<Integer, Double> entry : polSortat.entrySet()){
            int power = entry.getKey();
            double coef = entry.getValue();
            monomS.setCoef(coef);
            monomS.setPutere(power);
            addMonom(monomS);

        }
        return pol.entrySet().stream()
                .map(entry -> new Monom(entry.getKey(), entry.getValue()).toString())
                .collect(Collectors.joining(" + "));

    }

}
