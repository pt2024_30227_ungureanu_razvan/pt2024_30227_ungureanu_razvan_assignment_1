
package com.example.pt2024_30227_ungureanu_razvan_assignment_1;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import static com.example.pt2024_30227_ungureanu_razvan_assignment_1.Operatii.adunare;
public class HelloController {
    @FXML
    private TextField polinom1TextField;
    @FXML
    private TextField polinom2TextField;
    @FXML
    private Label resultLabel;
    @FXML
    private Button AdunareButton;
    @FXML
    private Button ScadereButton;
    @FXML
    private Button InmultireButton;

    @FXML
    private Button DerivareButton;
    @FXML
    private Button IntegrareButton;


        private Polinom parsePolynomial(String input) {
            Polinom polinom = new Polinom();
            String[] terms = input.split("(?=[-+])");

            for (String term : terms) {

                java.util.regex.Matcher matcher = java.util.regex.Pattern.compile("([-+]?\\d*\\.?\\d*)(?:\\*?x(?:\\^(\\d+))?)?").matcher(term);
                if (matcher.matches()) {
                String coefStr = matcher.group(1);
                String powerStr = matcher.group(2);

                double coef=0.0;
                int power=0;

                if(coefStr==null ||coefStr.isEmpty());
                else coef=Double.parseDouble(coefStr);
                if(powerStr==null ||powerStr.isEmpty());
                else power=Integer.parseInt(powerStr);

                Monom monom = new Monom(power, coef);
                polinom.addMonom(monom);
            }

        }

        return polinom;
    }


    public void adunare() {
        String polinom1Str = polinom1TextField.getText();
        String polinom2Str = polinom2TextField.getText();
        Polinom polinom1 = parsePolynomial(polinom1Str);
        Polinom polinom2 = parsePolynomial(polinom2Str);
        Polinom polinomrez;
        polinomrez=Operatii.adunare(polinom1,polinom2);
        resultLabel.setText(polinomrez.toString());

    }

    public void scadere() {
        String polinom1Str = polinom1TextField.getText();
        String polinom2Str = polinom2TextField.getText();
        Polinom polinom1 = parsePolynomial(polinom1Str);
        Polinom polinom2 = parsePolynomial(polinom2Str);
        Polinom polinomrez;
        polinomrez=Operatii.scadere(polinom1,polinom2);
        resultLabel.setText(polinomrez.toString());

    }


    public void derivare() {
        String polinom1Str = polinom1TextField.getText();
        Polinom polinom1 = parsePolynomial(polinom1Str);
        Polinom polinomrez;
        polinomrez=Operatii.derivare(polinom1);
        resultLabel.setText(polinomrez.toString());

    }

    public void integrare() {
        String polinom1Str = polinom1TextField.getText();
        Polinom polinom1 = parsePolynomial(polinom1Str);
        Polinom polinomrez;
        polinomrez=Operatii.integrare(polinom1);
        resultLabel.setText(polinomrez.toString());

    }
    public void inmultire(){
        String polinom1Str = polinom1TextField.getText();
        String polinom2Str = polinom2TextField.getText();
        Polinom polinom1 = parsePolynomial(polinom1Str);
        Polinom polinom2 = parsePolynomial(polinom2Str);
        Polinom polinomrez;
        polinomrez=Operatii.inmultire(polinom1,polinom2);
        resultLabel.setText(polinomrez.toString());

    }

}
