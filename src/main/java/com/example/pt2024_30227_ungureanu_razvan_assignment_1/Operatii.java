package com.example.pt2024_30227_ungureanu_razvan_assignment_1;

import java.util.*;

public class Operatii {

    public static Polinom adunare(Polinom p1, Polinom p2) {
        Polinom rezultat = new Polinom();
        rezultat.setPol(p1.getPol());

        for (Map.Entry<Integer, Double> entry : p2.getPol().entrySet()) {
            Monom monom = new Monom();
            int powerResult = entry.getKey();
            double coefResult = entry.getValue();
            monom.setCoef(coefResult);
            monom.setPutere(powerResult);
            if (rezultat.getPol().containsKey(monom.getPutere())) {

                coefResult += rezultat.getPol().get(powerResult);
            }
            rezultat.getPol().put(powerResult, coefResult);
        }
        return rezultat;
    }



    public static Polinom scadere(Polinom p1, Polinom p2) {
        Polinom rezultat = new Polinom();

        for (Map.Entry<Integer, Double> entry : p1.getPol().entrySet()) {
            int powerResult = entry.getKey();
            double coefResult = entry.getValue();
            if (p2.getPol().containsKey(powerResult)) {
                coefResult -= p2.getPol().get(powerResult);
            } else  if (!p1.getPol().containsKey(powerResult)){
                coefResult *= -1;
            }
            rezultat.getPol().put(powerResult, coefResult);
        }
        for (Map.Entry<Integer, Double> entry : p2.getPol().entrySet()) {
            int powerResult = entry.getKey();
            double coefResult = -entry.getValue();

            if (!p1.getPol().containsKey(powerResult)) {
                rezultat.getPol().put(powerResult, coefResult);
            }
        }
        return rezultat;
    }



    public static Polinom derivare(Polinom p1) {
        Polinom rezultat = new Polinom();

        for (Map.Entry<Integer, Double> entry : p1.getPol().entrySet()) {
            Monom monom=new Monom();
            int power = entry.getKey();
            double coef = entry.getValue();
            double var = coef * power;
            if(power-1>=0){
            monom.setPutere(power-1);
            monom.setCoef(var);
            rezultat.addMonom(monom);}
        }
        return rezultat;
    }


    public static Polinom integrare(Polinom p1) {
        Polinom rezultat = new Polinom();

        for (Map.Entry<Integer, Double> entry : p1.getPol().entrySet()) {
            Monom monom=new Monom();
            int power = entry.getKey();
            double coef = entry.getValue();

            double var = coef*1/(power+1);
            monom.setPutere(power+1);
            monom.setCoef(var);
            rezultat.addMonom(monom);
        }
        return rezultat;
    }


    public static Polinom inmultire(Polinom p1, Polinom p2) {
        Polinom rezultat = new Polinom();

        for (Map.Entry<Integer, Double> entry1 : p1.getPol().entrySet()) {
            int powerResult1 = entry1.getKey();
            double coefResult1 = entry1.getValue();
            for (Map.Entry<Integer, Double> entry2 : p2.getPol().entrySet()) {
                int powerResult2 = entry2.getKey();
                double coefResult2 = entry2.getValue();
                Monom monom=new Monom();

                double coefRez = coefResult1 * coefResult2;
                int powerRez = powerResult1 + powerResult2;
                monom.setCoef(coefRez);
                monom.setPutere(powerRez);
                rezultat.addMonom(monom);
            }
        }
        return rezultat;
    }
}
