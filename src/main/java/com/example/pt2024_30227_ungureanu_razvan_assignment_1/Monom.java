package com.example.pt2024_30227_ungureanu_razvan_assignment_1;

public class Monom {
    private int putere;
    private double coef;

    public Monom(int putere, double coef) {
        this.putere = putere;
        this.coef = coef;
    }

    public Monom() {

    }

    public int getPutere() {
        return putere;
    }

    public double getCoef() {
        return coef;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }

    @Override
    public String toString() {
        return  coef+"*"+"x"+"^"+putere;

    }
}
